var dayjs = require('dayjs')
var id = require('dayjs/locale/id')

/**
 * Fungsi untuk mengubah format tanggal dalam bahasa Indonesia.
 * @method
 * @param {string} tanggal - Teks tanggal dari server.
 * @returns {string} - Teks tanggal yang akan ditampilkan pada aplikasi.
 */
function konversiTanggal(tanggal) {
  return dayjs(tanggal).locale(id).format('D MMMM YYYY')
}

module.exports = konversiTanggal

/**
 * Fungsi untuk mengambil elemen terakhir pada sebuah array.
 * @method
 * @param {object} array - Array yang akan diambil elemen terakhirnya.
 * @returns {any} - Elemen terakhir dari array.
 */
function elemenTerakhir(array) {
  return array ? array.slice(-1).pop() : null
}

module.exports = elemenTerakhir

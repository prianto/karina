var test = require('ava')
var konversiTanggal = require('../../helper/konversi_tanggal')

/**
 * Pengujian fungsi "konversiTanggal".
 */
test('Menguji fungsi "konversiTanggal"', t => {
  t.is(konversiTanggal('2020-3-31'), '31 Maret 2020')

  t.is(konversiTanggal('2020-4-1'), '1 April 2020')

  t.is(konversiTanggal('2020-10-12'), '12 Oktober 2020')
})

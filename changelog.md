# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### 1.1.1 (2020-05-26)

### Improvements

- Miscellaneous.

### 1.1.0 (2020-05-26)

### Features

- Create web app.

### 1.0.2 (2020-05-25)

### Improvements

- Drop 'dirawat' var/def.

### 1.0.1 (2020-04-05)

### Improvements

- Update syntax.

### 1.0.0 (2020-04-03)

- Rilis awal.

var express = require('express')
var app = express()
var port = process.env.PORT || '3033'
var get = require('axios')

var elemenTerakhir = require('./helper/elemen_terakhir')
var konversiTanggal = require('./helper/konversi_tanggal')

var table = ['N/A', 'N/A', 'N/A']
var date = ''

async function getData() {
  await get('https://pomber.github.io/covid19/timeseries.json')
    .then(function (response) {
      // handle success
      if (response.data && 'Indonesia' in response.data) {
        var kasusIDN = response.data['Indonesia']
        var kasusTerbaruIDN = elemenTerakhir(kasusIDN)

        if (kasusTerbaruIDN) {
          table = [
            kasusTerbaruIDN.confirmed,
            kasusTerbaruIDN.recovered,
            kasusTerbaruIDN.deaths
          ]

          date = konversiTanggal(kasusTerbaruIDN.date)
        }
      }
    })
    .catch(function (error) {
      // handle error
      readyToDisplay = error
    })
    .then(function () {
      // always executed
    })
}

app.get('/', async (req, res) => {
  await getData()

  res.send(`<!DOCTYPE html>
    <html>
      <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Karina - Update Kasus Korona (Covid-19) di Indonesia</title>
        <meta property="og:title" content="Karina - Update Kasus Korona (Covid-19) di Indonesia">
        <meta property="og:description" content="Informasi kasus korona (covid-19) positif, sembuh, dan meninggal di Indonesia.">
        <meta name="description" content="Informasi kasus korona (covid-19) positif, sembuh, dan meninggal di Indonesia.">
        <link rel="icon" type="image/png" href=" data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAMAAACdt4HsAAACQ1BMVEUAAAChAAD///////+hAAD//v6lAwP/+vqzDAyrBwf/7+///v7//f2hAACiAQH//v7//PyjAgKlAwOmAwP/9fX//v6gAAD//////v7//v7//v6hAACiAQGiAQGiAQH//Pz/+vqjAQH//v6kAgKnBASlAwOtCAilAwP//Pz/7++wCgq2Dg7///+gAAD//v7//v7//v6hAACiAQGiAQGhAAChAACjAgKjAQGjAQGhAACjAgL/+vr/+vr//Pz//PymBASnBQX/7u7+vr6sCQmiAACiAAD//f2jAQH//f3//f2hAACiAQH//v7//PyiAQGlAwP//Pz/+vqjAQGnBAT/+fn/9fWhAAD/6en/9/f/4+P/7e3///+iAAD//////v7//v6hAAD///+jAgL//v7//PymAwP//PyjAgL//v7//v6lAwP//f3/+vr//Pz/+vqjAgL/+PikAwP/+fn/+fn/9PSmAwP//Pz/+/v/+vr//v7/4uL/////+/v///+nBAT//Pz//f3//v7//f23Dw+rBwf//f2hAAD+29v///+gAAC6ICD48fHHcnL8+Pj17+/07Oz9+/vu3d3s2trEl5eqTk7x5ublz8/hvr7fubnbr6/ZqqqxY2O/TU2lODjq0tLmysrXvLzVuLjcsrLNp6fKoaHSl5e9h4fKf3/FZ2etWVm7KCj69fX18PDs4eHs4ODm19fl1dXm0dHfy8vTtbXQrq7OrKzWoqLVoqLUnJzDlJTBj4/Pjo7MhYW3eXmoRUWnPz+iHR3ZSYMZAAAAiHRSTlMA/Pz68b4dGRMGBbVr9a+bcHBGLif5+fb08Ofk39fAkIBpZ2dgVyQiHRYNCv7m49vBt6ajnJmIeHNsY2JfW0w5GxQCAurbysbEvLuroZ+Uk2NRQT40MBYSDAkD7+3p1tDPt7Oxra2WjoSBfXx7dW1eV1JKRkUzLCEXDwvi4MK+uqmlko2DdSsdWuLcYQAABHFJREFUWMOM1MtLG0EcB/DvwLLsHhJIArnkASEvSOohT2MvCUQRFBQUPHkQQQQvxYPXXlrKfg+NpdFeRFAqgtD349BCH39aZ5wNSfdh9nP6wcz8GOY7M/B7vCqMAnwKy2K1OMRcw30qFjxMKk/rmOcJH2rAIuZZpJKHzyqVRcxjrlBKwCdBKbeDuexSllwKbLBSHiKKwQIb0LY3O0Vbl3lmdhDREpmCkqS0CSWltxWNlWZzGskBlBbTFiKLk1VIdp5cqECqknFEd0y2oVQES1A65DGisw2mk4vxLBWRaxb6aRo2EF2LPi0o1QoeUmuJvA3YvV365Mo6lvU6wq2RPMFJjoEaAzwj2UGoPqWthGCI5a02JXhsF6qTilKG4USM5Apc5lHJ1s/PMKGVOk16/R594lSulKxBq2fJtSFSguQhJo7odfbyA6dEEhOP9f/QpRR7AS1J19vzzxfULv/y3u1XSrsmNDNDaQ95KkbPgmQtUBs7jnPK/317TWUNitXTUwXi1MR+uY4Nur440hkDlVEvNwQ1A1tpZg4TcUEyti7oupHr3zFYtmlQiheXGhQFwEpBqm7GOOtq9P4Xw4l2BVKthqmdrsGIYhsD+Lm5XDLMn6trd7CPYOskr52fDPHRcd7oKoFgyyr/V7ecdXHBiZHjnOvqAIEG9PkhF43G1L47j27cHODX69b69Bo798bU7u7oAlKm7/z2ntPr1Ll3Sq/KBmMpzGozwJmj+a9khmQXs/41Ui+taURRHMAPM4jgCLpQfGwUVxFE3fsAXYhCBANKoiEJJSE05EFIQl5t6QNK/4xKVWg0IgkUQkK6bwvth8v13hkDzqjz29y7mLnn3nMOx4VXvdGTCN1WNW2YcJEgVIBECMKTqj6Aa6lcC9PKMrBPgpDfD9nPIPxX1Vtw3WeVee5i2nU2sJmjaR4IzdtBB0K3xeIb/w+QOS+Mmk0YJcmcDRa9JXNhzNNsjXoQsmTGHvJhnp+q+gjBv7tEr+prPIMuL+a7UdU/0MllDw96SVSRJcUZ2ZEA+Eqif+5/wUzvriPif/aDUY7cRUgRCkCQlMw1fQDTVwdgOv2/NzNSWD/eliEEKSbWr7wvzmXeB7x0Q9ZDdzCK8yfnnEGMeanA/9eTsgdO7+MhjE5JsL8DUyQnmArpStD9ZgfcwyBMuhSYCOVtQLxOgqN64oOmPXzsg/sx+idBE0hnL/WPE4CtwdbUwaSqW7M64DsECUDQQUL0ICW2E1Ew5ZKhBx8GEEK8WVdppiSAo/yeBFPrKxQBsE2z2beAE6JaEiaWo0RVCckrmuesypePMPhEYxc1smLNC1/YJkPjVw4lrDvIugzwhShfO96FL3LqEVN3hax7A5mX9moTCIl5F8MOWRaVtKG9DCarbb2rZJUbMQ/fKIB+9XPASRZdbEAh7purUHRPnrVhJysc6bhpMDcQTzdoIXsCjNvsACax+BJhjNlmDf1DWsQJbslQGXCZxSngkWRDyXISGKVBi6Vt/vcmgZwBqZAigxdxMfBftMsJEQAAAABJRU5ErkJggg==" />
        <meta property="og:image" content=" data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAMAAACdt4HsAAACQ1BMVEUAAAChAAD///////+hAAD//v6lAwP/+vqzDAyrBwf/7+///v7//f2hAACiAQH//v7//PyjAgKlAwOmAwP/9fX//v6gAAD//////v7//v7//v6hAACiAQGiAQGiAQH//Pz/+vqjAQH//v6kAgKnBASlAwOtCAilAwP//Pz/7++wCgq2Dg7///+gAAD//v7//v7//v6hAACiAQGiAQGhAAChAACjAgKjAQGjAQGhAACjAgL/+vr/+vr//Pz//PymBASnBQX/7u7+vr6sCQmiAACiAAD//f2jAQH//f3//f2hAACiAQH//v7//PyiAQGlAwP//Pz/+vqjAQGnBAT/+fn/9fWhAAD/6en/9/f/4+P/7e3///+iAAD//////v7//v6hAAD///+jAgL//v7//PymAwP//PyjAgL//v7//v6lAwP//f3/+vr//Pz/+vqjAgL/+PikAwP/+fn/+fn/9PSmAwP//Pz/+/v/+vr//v7/4uL/////+/v///+nBAT//Pz//f3//v7//f23Dw+rBwf//f2hAAD+29v///+gAAC6ICD48fHHcnL8+Pj17+/07Oz9+/vu3d3s2trEl5eqTk7x5ublz8/hvr7fubnbr6/ZqqqxY2O/TU2lODjq0tLmysrXvLzVuLjcsrLNp6fKoaHSl5e9h4fKf3/FZ2etWVm7KCj69fX18PDs4eHs4ODm19fl1dXm0dHfy8vTtbXQrq7OrKzWoqLVoqLUnJzDlJTBj4/Pjo7MhYW3eXmoRUWnPz+iHR3ZSYMZAAAAiHRSTlMA/Pz68b4dGRMGBbVr9a+bcHBGLif5+fb08Ofk39fAkIBpZ2dgVyQiHRYNCv7m49vBt6ajnJmIeHNsY2JfW0w5GxQCAurbysbEvLuroZ+Uk2NRQT40MBYSDAkD7+3p1tDPt7Oxra2WjoSBfXx7dW1eV1JKRkUzLCEXDwvi4MK+uqmlko2DdSsdWuLcYQAABHFJREFUWMOM1MtLG0EcB/DvwLLsHhJIArnkASEvSOohT2MvCUQRFBQUPHkQQQQvxYPXXlrKfg+NpdFeRFAqgtD349BCH39aZ5wNSfdh9nP6wcz8GOY7M/B7vCqMAnwKy2K1OMRcw30qFjxMKk/rmOcJH2rAIuZZpJKHzyqVRcxjrlBKwCdBKbeDuexSllwKbLBSHiKKwQIb0LY3O0Vbl3lmdhDREpmCkqS0CSWltxWNlWZzGskBlBbTFiKLk1VIdp5cqECqknFEd0y2oVQES1A65DGisw2mk4vxLBWRaxb6aRo2EF2LPi0o1QoeUmuJvA3YvV365Mo6lvU6wq2RPMFJjoEaAzwj2UGoPqWthGCI5a02JXhsF6qTilKG4USM5Apc5lHJ1s/PMKGVOk16/R594lSulKxBq2fJtSFSguQhJo7odfbyA6dEEhOP9f/QpRR7AS1J19vzzxfULv/y3u1XSrsmNDNDaQ95KkbPgmQtUBs7jnPK/317TWUNitXTUwXi1MR+uY4Nur440hkDlVEvNwQ1A1tpZg4TcUEyti7oupHr3zFYtmlQiheXGhQFwEpBqm7GOOtq9P4Xw4l2BVKthqmdrsGIYhsD+Lm5XDLMn6trd7CPYOskr52fDPHRcd7oKoFgyyr/V7ecdXHBiZHjnOvqAIEG9PkhF43G1L47j27cHODX69b69Bo798bU7u7oAlKm7/z2ntPr1Ll3Sq/KBmMpzGozwJmj+a9khmQXs/41Ui+taURRHMAPM4jgCLpQfGwUVxFE3fsAXYhCBANKoiEJJSE05EFIQl5t6QNK/4xKVWg0IgkUQkK6bwvth8v13hkDzqjz29y7mLnn3nMOx4VXvdGTCN1WNW2YcJEgVIBECMKTqj6Aa6lcC9PKMrBPgpDfD9nPIPxX1Vtw3WeVee5i2nU2sJmjaR4IzdtBB0K3xeIb/w+QOS+Mmk0YJcmcDRa9JXNhzNNsjXoQsmTGHvJhnp+q+gjBv7tEr+prPIMuL+a7UdU/0MllDw96SVSRJcUZ2ZEA+Eqif+5/wUzvriPif/aDUY7cRUgRCkCQlMw1fQDTVwdgOv2/NzNSWD/eliEEKSbWr7wvzmXeB7x0Q9ZDdzCK8yfnnEGMeanA/9eTsgdO7+MhjE5JsL8DUyQnmArpStD9ZgfcwyBMuhSYCOVtQLxOgqN64oOmPXzsg/sx+idBE0hnL/WPE4CtwdbUwaSqW7M64DsECUDQQUL0ICW2E1Ew5ZKhBx8GEEK8WVdppiSAo/yeBFPrKxQBsE2z2beAE6JaEiaWo0RVCckrmuesypePMPhEYxc1smLNC1/YJkPjVw4lrDvIugzwhShfO96FL3LqEVN3hax7A5mX9moTCIl5F8MOWRaVtKG9DCarbb2rZJUbMQ/fKIB+9XPASRZdbEAh7purUHRPnrVhJysc6bhpMDcQTzdoIXsCjNvsACax+BJhjNlmDf1DWsQJbslQGXCZxSngkWRDyXISGKVBi6Vt/vcmgZwBqZAigxdxMfBftMsJEQAAAABJRU5ErkJggg==">
        <style>
          html, body {
            height: 100%;
          }
          body {
            margin: 0;
            font-family: Arial Narrow, Arial, Helvetica, sans-serif;
            background: #200122;  /* fallback for old browsers */
            background: -webkit-linear-gradient(to top, #6f0000, #200122);  /* Chrome 10-25, Safari 5.1-6 */
            background: linear-gradient(to top, #6f0000, #200122); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
          }
          .flex-container {
            height: 100%;
            padding: 0;
            margin: 0;
            display: -webkit-box;
            display: -moz-box;
            display: -ms-flexbox;
            display: -webkit-flex;
            display: flex;
            align-items: center;
            justify-content: center;
          }
          .row {
            width: auto;
          }
          .flex-item {
            background-color: rgba(227, 218, 220, 1);
            border-radius:15px;
            color: #444;
            padding: 5px;
            width: 320px;
            height: 200px;
            margin: 10px;
            line-height: 20px;
            font-weight: bold;
            font-size: 2em;
            text-align: center;
          }
          h2 {
            font-size: 18px;
            font-weight: bold;
          }
          p {
            font-size: 16px;
            font-weight: normal;
          }
          table {
            font-size: 14px;
            font-weight: normal;
            width: 80%;
            margin: 0 10%;
            border: 1px dotted #999;
            border-collapse: collapse;
          }
          table tr td {
            width: 50%;
            border: 1px dotted #999;
            padding: 5px 10px;
          }
          table tr td:first-child {
            text-align: right;
          }
          table tr td:last-child {
            text-align: left;
          }
        </style>
      </head>
      <body>
        <div class="flex-container">
          <div class="row"> 
            <div id="app" class="flex-item">
              <h2>Update Kasus Covid-19 di Indonesia</h2>
              <p>Per ${date}</p>
              <table>
                <tr>
                  <td>Positif</td>
                  <td id="confirmed">${table[0]}</td>
                </tr>
                <tr>
                  <td>Sembuh</td>
                  <td id="recovered">${table[1]}</td>
                </tr>
                <tr>
                  <td>Meninggal</td>
                  <td id="deaths">${table[2]}</td>
                </tr>
              </table>
            </div>
          </div>
        </div>
      </body>
    </html>`)
})

app.listen(port, '0.0.0.0', () =>
  console.log(`Example app listening at http://0.0.0.0:${port}`)
)

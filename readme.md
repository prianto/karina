![vismon](favicon.png 'Karina')

# Karina

![codeship](https://app.codeship.com/projects/5d2b2ed0-576e-0138-236c-1e5141835944/status?branch=master 'Build Status')

Informasi kasus korona (covid-19) terbaru di Indonesia.

> Aplikasi ini dibuat sebagai materi _knowledge sharing_ mengenai pengujian perangkat lunak.

## Install

```sh
npm install
```

## Test

```sh
npm test
```

## Build

```sh
npm run build
```

module.exports = {
  'Web Karina': function (browser) {
    browser
      .url('http://localhost:3033')
      .waitForElementVisible('body')
      .pause(1000)
      .getCssProperty('#app', 'background-color', function (result) {
        this.assert.equal(result.value.trim(), 'rgba(227, 218, 220, 1)')
      })
      .getText('p', function (result) {
        this.assert.notEqual(result.value.trim(), 'Per')
      })
      .getText('#confirmed', function (result) {
        this.assert.notEqual(result.value.trim(), 'N/A')
        this.assert.ok(result.value.trim() >= 0)
      })
      .getText('#recovered', function (result) {
        this.assert.notEqual(result.value.trim(), 'N/A')
        this.assert.ok(result.value.trim() >= 0)
      })
      .getText('#deaths', function (result) {
        this.assert.notEqual(result.value.trim(), 'N/A')
        this.assert.ok(result.value.trim() >= 0)
      })
      .end()
  }
}

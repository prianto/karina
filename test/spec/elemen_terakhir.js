var test = require('ava')
var elemenTerakhir = require('../../helper/elemen_terakhir')

/**
 * Pengujian fungsi "elemenTerakhir".
 */
test('Menguji fungsi "elemenTerakhir"', t => {
  t.is(elemenTerakhir([0, 1, 2, 3]), 3)

  t.is(elemenTerakhir(['lorem', 'ipsum', 'dolor', 'amet']), 'amet')

  t.deepEqual(
    elemenTerakhir([
      [1, 2],
      ['a', 'b'],
      [3, 4],
      ['c', 'd']
    ]),
    ['c', 'd']
  )
})
